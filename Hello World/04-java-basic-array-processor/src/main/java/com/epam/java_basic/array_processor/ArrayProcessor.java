package com.epam.java_basic.array_processor;

/**
 * Useful methods for array processing
 */

import static com.epam.java_basic.array_processor.ArrayModel.*;

public class ArrayProcessor {
    // input source array
    private final int[] sourceArray = {11, 6, 9, 23, 2, -4, -6, -7, -11, 34, -6, 10, 3, -4, -5, -1, 4, -32, 63, 23};
    private static final String START_MESSAGE = "Source array = ";

    private static final String FIRST_MESSAGE = "Swap Max Negative And Min Positive Elements";
    private static final String SECOND_MESSAGE = "Сount Sum Of Elements On Even Positions";
    private static final String THIRD_MESSAGE = "Replace Each Negative Elements With Zero";
    private static final String FOURTH_MESSAGE = "Multiply By Three Each Positive Element Standing Before Negative";
    private static final String FIFTH_MESSAGE = "Calculate Difference Between Average And Min Element";
    private static final String SIXTH_MESSAGE = "Find Same Elements Standing On Odd Positions";

    public void workApplication() {
        ArrayProcessor app = new ArrayProcessor();
        ArrayView.printMessage(START_MESSAGE);
        ArrayView.printArrayConsol(sourceArray);
        ArrayView.printArrayConsol(app.swapMaxNegativeAndMinPositiveElements(sourceArray));
        ArrayView.prinNumberConsolInt(app.countSumOfElementsOnEvenPositions(sourceArray));
        ArrayView.printArrayConsol(app.replaceEachNegativeElementsWithZero(sourceArray));
        ArrayView.printArrayConsol(app.multiplyByThreeEachPositiveElementStandingBeforeNegative(sourceArray));
        ArrayView.prinNumberConsolFloat(app.calculateDifferenceBetweenAverageAndMinElement(sourceArray));
        ArrayView.printArrayConsol(app.findSameElementsStandingOnOddPositions(sourceArray));
    }

    public int[] swapMaxNegativeAndMinPositiveElements(int[] input) {
        ArrayView.printMessage(FIRST_MESSAGE);
        return chengeElementArray(input);
    }

    public int countSumOfElementsOnEvenPositions(int[] input) {
        ArrayView.printMessage(SECOND_MESSAGE);
        return sumEvenPositions(input);
    }

    public int[] replaceEachNegativeElementsWithZero(int[] input) {
        ArrayView.printMessage(THIRD_MESSAGE);
        return negativeElementsWithZero(input);
    }

    public int[] multiplyByThreeEachPositiveElementStandingBeforeNegative(int[] input) {
        ArrayView.printMessage(FOURTH_MESSAGE);
        return multiplyByThree(input);
    }

    public float calculateDifferenceBetweenAverageAndMinElement(int[] input) {
        ArrayView.printMessage(FIFTH_MESSAGE);
        return calculateDifference(input);
    }

    public int[] findSameElementsStandingOnOddPositions(int[] input) {
        ArrayView.printMessage(SIXTH_MESSAGE);
        return findSameElements(input);
    }
}



