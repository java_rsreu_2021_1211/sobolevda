package com.epam.java_basic;

import com.epam.java_basic.array_processor.ArrayProcessor;

/**
 * Application's entry point, use it to demonstrate your code execution
 */

public class Application {
    public static void main(String[] args) {
        ArrayProcessor action = new ArrayProcessor();
        action.workApplication();
    }
}
