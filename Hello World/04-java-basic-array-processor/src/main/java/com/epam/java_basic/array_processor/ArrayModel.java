package com.epam.java_basic.array_processor;

public class ArrayModel {
    private ArrayModel() {
    }

    private static final Integer DIVISION_TWO = 2;
    private static final Integer CORRECT_ELEMENT_ARRAY = 1;
    private static final Integer MULTIPLY = 3;
    private static final Integer MULTIPLY_TEN = 10;

    public static int[] chengeElementArray(int[] input) {
        int maxNegative = input[0];
        int minPositive = input[0];
        int minNumber = 0; // index number element
        int maxNumber = 0; // index number element

        //find min & max element array
        for (int i = 0; i < input.length; i++) {
            if (input[i] < maxNegative) {
                maxNegative = input[i];
            }
            if (input[i] > minPositive) {
                minPositive = input[i];
            }
        }
        //find max negative, min positive and their checkpoint
        for (int i = 0; i < input.length; i++) {
            if ((input[i] > maxNegative) && (input[i] < 0)) {
                maxNegative = input[i];
                maxNumber = i;
            }
            if ((input[i] > 0) && (input[i] < minPositive)) {
                minPositive = input[i];
                minNumber = i;
            }
        }
        int[] newArray = input.clone();
        newArray[minNumber] = maxNegative;
        newArray[maxNumber] = minPositive;
        return newArray;
    }

    public static int sumEvenPositions(int[] input) {
        int summEvenPosition = 0;
        for (int i = 0; i < input.length; i++) {
            if (i % DIVISION_TWO == 0) {
                summEvenPosition += input[i];
            }
        }
        return summEvenPosition;
    }

    public static int[] negativeElementsWithZero(int[] input) {
        int[] zeroArray = input.clone();
        for (int i = 0; i < zeroArray.length; i++) {
            if (zeroArray[i] < 0) {
                zeroArray[i] = 0;
            }
        }
        return zeroArray;
    }

    public static int[] multiplyByThree(int[] input) {
        int[] multiplyArray = input.clone();
        for (int i = 0; i < multiplyArray.length - CORRECT_ELEMENT_ARRAY; i++) {
            if ((multiplyArray[i] > 0) && (multiplyArray[i + CORRECT_ELEMENT_ARRAY] < 0)) {
                multiplyArray[i] *= MULTIPLY;
            }
        }
        return multiplyArray;
    }

    public static float calculateDifference(int[] input) {
        int minElementArray = input[0];
        float summArifmetic = 0f;
        for (int i = 1; i < input.length; i++) {
            if (input[i] < minElementArray) {
                minElementArray = input[i];
            }
            summArifmetic = summArifmetic + (float) input[i];
        }
        int helpNumber = (int) ((summArifmetic / (input.length + 1) - minElementArray) * MULTIPLY_TEN);
        summArifmetic = (float) helpNumber / MULTIPLY_TEN;
        return summArifmetic;
    }

    public static int[] findSameElements(int[] input) {
        int[] newArray = new int[input.length / DIVISION_TWO + CORRECT_ELEMENT_ARRAY];
        int numberElementNewArray = -1;

        for (int i = 0; i < input.length; i++) {
            for (int j = i + 1; j < input.length - 1; j++) {
                if ((checkQualityAndParity(input[i], input[j], i, j)) &&
                        (!checkRepeatElement(numberElementNewArray, input[i], newArray))) {
                    //checking the repeat element
                    numberElementNewArray++;
                    newArray[numberElementNewArray] = input[i];
                }
            }
        }
        int[] resultArray = new int[numberElementNewArray + CORRECT_ELEMENT_ARRAY];
        if (numberElementNewArray != -1) {
            System.arraycopy(newArray, 1, resultArray, 1, numberElementNewArray);
        }
        return resultArray;
    }

    private static boolean checkRepeatElement(int countElementArray, int count, int[] newArray) {
        boolean bol = false;
        for (int i = 0; i < countElementArray + CORRECT_ELEMENT_ARRAY; i++) {
            if (newArray[i] == count) {
                bol = true;
                break;
            }
        }
        return bol;
    }

    private static boolean checkQualityAndParity(int number1, int number2, int i, int j) {
        return ((number1 == number2) && ((i % DIVISION_TWO != 0) || (j % DIVISION_TWO != 0)));
    }

}
