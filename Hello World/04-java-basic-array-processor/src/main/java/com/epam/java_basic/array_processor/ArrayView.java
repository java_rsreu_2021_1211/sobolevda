package com.epam.java_basic.array_processor;

public class ArrayView {

    public static void printMessage(String message) {
        System.out.printf("%n" + message + "%n");
    }

    public static void printArrayConsol(int[] input) {
        for (int i = 0; i < input.length; i++) {
            System.out.printf(input[i] + " ");
        }
        System.out.printf("%n");
    }

    public static void prinNumberConsolInt(int number) {
        System.out.println(number);
    }

    public static void prinNumberConsolFloat(float number) {
        System.out.println(number);
    }

    private ArrayView() {
    }

}
