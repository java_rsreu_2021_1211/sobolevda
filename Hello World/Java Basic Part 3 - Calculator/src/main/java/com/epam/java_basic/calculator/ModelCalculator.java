package com.epam.java_basic.calculator;

import java.util.Locale;
import java.util.Scanner;

public class ModelCalculator {
    private ModelCalculator() {
    }

    private static final String ERR_DIGITS = "Input positive digits!";
    private static final String ERR_OPERATOR = "Enter only one of the (+, -, *, /)";
    private static final String CORRECT_INPUT = "Enter only Y or N ";
    private static final int CORRECT_MANTISSA = 10;

    public static int readDigitsInt() {
        int readNumber = 0;
        Scanner scaner = new Scanner(System.in);
        boolean numberTrue = false;
        while (!numberTrue) {
            if (scaner.hasNextInt()) {
                readNumber = Integer.parseInt(scaner.nextLine());
                if (readNumber >= 0) {
                    numberTrue = true;
                } else {
                    ViewCalculator.message(ERR_DIGITS);
                }
            } else {
                ViewCalculator.message(ERR_DIGITS);
                scaner.nextLine();
            }
        }
        return readNumber;
    }

    public static double readDigitsDouble() {
        double readNumber = 0.0f;
        Scanner scaner = new Scanner(System.in);
        scaner.useLocale(Locale.US);// change of location for double (, vs .)
        boolean numberTrue = false;
        while (!numberTrue) {
            if (scaner.hasNextDouble()) {
                readNumber = Double.parseDouble(scaner.nextLine());
                if (readNumber >= 0) {
                    numberTrue = true;
                } else {
                    ViewCalculator.message(ERR_DIGITS);
                }
            } else {
                ViewCalculator.message(ERR_DIGITS);
                scaner.nextLine();
            }
        }
        return readNumber;
    }

    public static String readOperator() {
        Scanner scaner = new Scanner(System.in);
        String operator = scaner.nextLine();
        boolean operatorTrue = false;
        while (!operatorTrue) {
            if (operator.equals("+")) {
                operatorTrue = true;
            }
            if (operator.equals("-")) {
                operatorTrue = true;
            }
            if (operator.equals("*")) {
                operatorTrue = true;
            }
            if (operator.equals("/")) {
                operatorTrue = true;
            }
            if (!operatorTrue) {
                ViewCalculator.message(ERR_OPERATOR);
                operator = scaner.nextLine();
            }
        } //while
        return operator;
    }

    public static Double answer(int mantissa, double result) {
        int precision = 1;
        for (int i = 0; i < mantissa; i++) {
            precision = precision * CORRECT_MANTISSA;
        }
        int helpNumber = (int) ((result) * precision);
        double resultAnswer = helpNumber;
        resultAnswer = resultAnswer / precision;
        return resultAnswer;
    }

    public static Boolean readContinue() {
        String continueLine = "";
        Scanner scaner = new Scanner(System.in);
        boolean correctAnswer = false;
        while (!correctAnswer) {
            continueLine = scaner.nextLine();

            if (continueLine.equals("Y")) {
                correctAnswer = true;
            }
            if (continueLine.equals("N")) {
                correctAnswer = true;
            }
            if (!correctAnswer) {
                ViewCalculator.message(CORRECT_INPUT);
            }
        }
        return continueLine.equals("Y");
    }

    public static Double addMod(double a, double b) {
        return a + b;
    }

    public static Double subMod(double a, double b) {
        return a - b;
    }

    public static Double multMod(double a, double b) {
        return a * b;
    }

    public static Double divMod(double a, double b) {
        return a / b;
    }

}
