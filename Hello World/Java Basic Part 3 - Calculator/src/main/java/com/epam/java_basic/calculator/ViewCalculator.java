package com.epam.java_basic.calculator;

public class ViewCalculator {
    private ViewCalculator() {
    }

    public static void message(String message) {
        System.out.println(message);
    }

    public static void printResult (String line, double number){
        System.out.println(line + number);
    }


}
