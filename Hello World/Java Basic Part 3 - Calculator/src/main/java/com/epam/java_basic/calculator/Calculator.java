package com.epam.java_basic.calculator;

public class Calculator {

    private static final String HELLO = "Hello!";
    private static final String MANTISSA = "Input length mantissa:";
    private static final String FIRST_LINE = "Enter the first number:";
    private static final String SECOND_LINE = "Enter the second number:";
    private static final String OPERATOR_LINE = "Enter operator (+, -, *, /):";
    private static final String ERR_BY_ZERO = "You can not divide by zero!";
    private static final String RESULT_LINE = "Result: ";
    private static final String CONTINUE_LINE = "Do you want to continue? (Y/N)";
    private static final String BYE_LINE = "Bye!";

    public static void workCalculator() {
        ViewCalculator.message(HELLO);
        for (int i = 0; ; i++) {
            ViewCalculator.message(MANTISSA);
            new Calculator(ModelCalculator.readDigitsInt());
            if (!ModelCalculator.readContinue()) {
                break;
            }
        }
        ViewCalculator.message(BYE_LINE);
    }

    public Calculator(int precision) {
        ViewCalculator.message(FIRST_LINE);
        double firstNumber = ModelCalculator.readDigitsDouble();
        ViewCalculator.message(SECOND_LINE);
        double secondNumber = ModelCalculator.readDigitsDouble();
        ViewCalculator.message(OPERATOR_LINE);
        String operator = ModelCalculator.readOperator();

        double result = 0d;

        if (operator.equals("+")) {
            result = add(firstNumber, secondNumber);
        }
        if (operator.equals("-")) {
            result = subtract(firstNumber, secondNumber);
        }
        if (operator.equals("*")) {
            result = multiply(firstNumber, secondNumber);
        }
        if (operator.equals("/")) {
            if (secondNumber != 0) {
                result = div(firstNumber, secondNumber);
            } else {
                ViewCalculator.message(ERR_BY_ZERO);
            }
        }

        if (!operator.equals("/") && (secondNumber != 0)) {
            ViewCalculator.printResult(RESULT_LINE, ModelCalculator.answer(precision, result));
        }
        ViewCalculator.message(CONTINUE_LINE);
    }

    public double add(double a, double b) {
        return ModelCalculator.addMod(a, b);
    }

    public double subtract(double a, double b) {
        return ModelCalculator.subMod(a, b);
    }

    public double multiply(double a, double b) {
        return ModelCalculator.multMod(a, b);
    }

    public double div(double a, double b) {
        return ModelCalculator.divMod(a, b);
    }
}
